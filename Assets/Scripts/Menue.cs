﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;

public class Menue : MonoBehaviour
{
    public GameObject mainmenu;
    public GameObject creddits;
    public GameObject settings;
    public AudioMixer audioMixer;

    public void PlayGame()
    {
        SceneManager.LoadScene(1);
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void Creddits()
    {
        mainmenu.SetActive(false);
        creddits.SetActive(true);
    }

    public void Settings()
    {
        mainmenu.SetActive(false);
        settings.SetActive(true);
    }

    public void Back()
    {
        settings.SetActive(false);
        creddits.SetActive(false);
        mainmenu.SetActive(true);
    }

    public void SetVlume (float volume)
    {
        audioMixer.SetFloat("volume", volume);
    }
    public void SXVvolume(float sxf)
    {
        audioMixer.SetFloat("sxf", sxf);
    }
    public void Musicvolume(float music)
    {
        audioMixer.SetFloat("music", music);
    }
}

