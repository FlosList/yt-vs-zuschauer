﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;


public class Followersystem : MonoBehaviour
{
    [SerializeField]
    public Transform firstPosition;
    public Transform[] erstePosition;
    public Transform[] zweitePosition;
    public Transform[] drittePosition;
    public Transform[] virtePosition;
    public Transform ziel;

    NavMeshAgent _navMeshAgent;

    public int liveZoneone;
    public int liveZonetwo;
    public int liveZonthree;
    public int health;

    public int zoneZaehler;
    public int zoneStop;
    public int notEnter;

    public int rand;

    private void Start()
    {
        _navMeshAgent = this.GetComponent<NavMeshAgent>();

        if(_navMeshAgent == null)
        {
            Debug.LogError("agent nor working on thit agent " + gameObject.name);
        }
        else
        {
            SetFirstDestination();
        }
    }

    private void Update()
    {
        rand = Random.Range(1,5);
        
        if(zoneZaehler == 1 && zoneStop == 0)
        {
            Set1Position();
        }
        if (zoneZaehler == 2 && zoneStop == 0)
        {
            Set2Position();
        }
        if (zoneZaehler == 3 && zoneStop == 0)
        {
            Set3Position();
        }
        if (zoneZaehler == 4 && zoneStop == 0)
        {
            Set4Position();
        }
        if (zoneZaehler == 5 && zoneStop == 0)
        {
            SetZiel();
        }
    }

    private void SetFirstDestination()
    {
        if(firstPosition != null)
        {
            Vector3 targetVector = firstPosition.transform.position;
            _navMeshAgent.SetDestination(targetVector);
        }
    }

    private void Set1Position()
    {
        zoneStop = 1;
        switch (rand)
        {
            case 1:
                Vector3 targetVector = erstePosition[0].transform.position;
                _navMeshAgent.SetDestination(targetVector);
                break;
            case 2:
                Vector3 targetVector1 = erstePosition[1].transform.position;
                _navMeshAgent.SetDestination(targetVector1);
                break;
            case 3:
                Vector3 targetVector2 = erstePosition[2].transform.position;
                _navMeshAgent.SetDestination(targetVector2);
                break;
            case 4:
                Vector3 targetVector3 = erstePosition[3].transform.position;
                _navMeshAgent.SetDestination(targetVector3);
                break;
        }
    }

    private void Set2Position()
    {
        zoneStop = 1;
        switch (rand)
        {
            case 1:
                Vector3 targetVector = zweitePosition[0].transform.position;
                _navMeshAgent.SetDestination(targetVector);
                break;
            case 2:
                Vector3 targetVector1 = zweitePosition[1].transform.position;
                _navMeshAgent.SetDestination(targetVector1);
                break;
            case 3:
                Vector3 targetVector2 = zweitePosition[2].transform.position;
                _navMeshAgent.SetDestination(targetVector2);
                break;
            case 4:
                Vector3 targetVector3 = zweitePosition[3].transform.position;
                _navMeshAgent.SetDestination(targetVector3);
                break;
        }
    }

    private void Set3Position()
    {
        zoneStop = 1;
        switch (rand)
        {
            case 1:
                Vector3 targetVector = drittePosition[0].transform.position;
                _navMeshAgent.SetDestination(targetVector);
                break;
            case 2:
                Vector3 targetVector1 = drittePosition[1].transform.position;
                _navMeshAgent.SetDestination(targetVector1);
                break;
            case 3:
                Vector3 targetVector2 = drittePosition[2].transform.position;
                _navMeshAgent.SetDestination(targetVector2);
                break;
            case 4:
                Vector3 targetVector3 = drittePosition[3].transform.position;
                _navMeshAgent.SetDestination(targetVector3);
                break;
        }
    }

    private void Set4Position()
    {
        zoneStop = 1;
        switch (rand)
        {
            case 1:
                Vector3 targetVector = virtePosition[0].transform.position;
                _navMeshAgent.SetDestination(targetVector);
                break;
            case 2:
                Vector3 targetVector1 = virtePosition[1].transform.position;
                _navMeshAgent.SetDestination(targetVector1);
                break;
            case 3:
                Vector3 targetVector2 = virtePosition[2].transform.position;
                _navMeshAgent.SetDestination(targetVector2);
                break;
            case 4:
                Vector3 targetVector3 = virtePosition[3].transform.position;
                _navMeshAgent.SetDestination(targetVector3);
                break;
        }
    }

    private void SetZiel()
    {
        if (ziel != null)
        {
            Vector3 targetVector = ziel.transform.position;
            _navMeshAgent.SetDestination(targetVector);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Position")
        {
            Debug.Log("ich bin getrrigert");
            if (zoneStop == 1 && notEnter == 0)
            {
                zoneZaehler++;
                zoneStop = 0;
                notEnter = 1;
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Position")
        {
            if (notEnter == 1)
            {
                notEnter = 0;
            }
        }
    }
}
