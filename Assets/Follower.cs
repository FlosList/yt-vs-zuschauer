﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Utilities;

[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(Rigidbody))]
internal class Follower:MonoBehaviour {

#pragma warning disable CS0649
    /// <summary>
    /// The first <see cref="Transform"/> to visit.<br/>
    /// </summary>
    [SerializeField]
    private Transform firstZone;
    /// <summary>
    /// Any intermediate <see cref="Transform"/>s to visit<br/>
    /// (After the first)
    /// </summary>
    [SerializeField]
    public Transform1D[] UnityInspector_intermediateZones;
    [Serializable]
    public class Transform1D{
        static System.Random random = new System.Random();
        [SerializeField]
        public Transform[] transforms;
        /// <summary>
        /// Returns a random element from the <see cref="Transform"/> array
        /// </summary>
        internal Transform Random() {
            return transforms[random.Next(transforms.Length)];
        }
    }
    private List<Transform1D> intermediateZones;
    /// <summary>
    /// The last <see cref="Transform"/> to visit<br/>
    /// (After the intermediates)
    /// </summary>
    [Space]
    [SerializeField]
    private Transform lastZone;
    /// <summary>
    /// The current target <see cref="Transform"/> of the <see cref="navMeshAgent"/>
    /// </summary>
    private Transform currentTarget;
#pragma warning restore CS0649

    /// <summary>
    /// <see cref="NavMeshAgent"/> on this <see cref="GameObject"/>
    /// </summary>
    private NavMeshAgent navMeshAgent {
        get {
            if(_navMeshAgent == null) {
                _navMeshAgent = GetComponent<NavMeshAgent>();
            }
            return _navMeshAgent;
        }
    }
    private NavMeshAgent _navMeshAgent = null;

#pragma warning disable IDE0051
    /// <summary>
    /// Unity calls this after the <see cref="Follower"/> is constructed.
    /// </summary>
    private void Start() {
        intermediateZones = new List<Transform1D>(UnityInspector_intermediateZones);
        SetNext();
    }
    /// <summary>
    /// Unity calls this whenever the <see cref="Rigidbody"/> on this <see cref="GameObject"/> becomes part of a <see cref="Collision"/>
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other) {
        if(other.tag == "Position" && other.transform == currentTarget) {
            Debug.Log("Zone entered: " + other.GetFullPath());
            SetNext();
        }
    }
#pragma warning restore IDE0051
    /// <summary>
    /// Sets the next target <see cref="Transform"/>
    /// </summary>
    private void SetNext() {
        if(firstZone != null) {
            currentTarget = firstZone;
            firstZone = null;
            navMeshAgent.SetDestination(currentTarget.position);
        } else if(intermediateZones.Count > 0) {
            currentTarget = intermediateZones[0].Random();
            intermediateZones.RemoveAt(0);
            navMeshAgent.SetDestination(currentTarget.position);
        } else if(lastZone != null) {
            currentTarget = lastZone;
            lastZone = null;
            navMeshAgent.SetDestination(currentTarget.position);
        } else {
            Celebrate();
        }
    }
    /// <summary>
    /// Congratulations for reaching all targets!
    /// </summary>
    private void Celebrate() {
        //Celebrate here
    }
}